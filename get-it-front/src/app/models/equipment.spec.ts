import { Equipment, Status } from './equipment';

let equipment : Equipment;

beforeEach(() => {
  // Créez une instance du modèle avant chaque test
  equipment = new Equipment();
});

describe('Equipment', () => {
  it('should create an instance', () => {
    expect(equipment).toBeTruthy();
  });
});

it('should set ref , type , status , brand and location correctly', () => {
  const ref = 'test 12345';
  const type = 'pc';
  const status = Status.ASSIGNED;
  const brand = "asus";
  const location = "Paris";

  equipment.ref = ref;
  equipment.type = type;
  equipment.status = status;
  equipment.brand = brand;
  equipment.location = location

  expect(equipment.ref).toEqual(ref);
  expect(equipment.type).toEqual(type);
  expect(equipment.status).toEqual(status);
  expect(equipment.brand).toEqual(brand);
  expect(equipment.location).toEqual(location);
  
});



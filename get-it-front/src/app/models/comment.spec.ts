import { Comment } from './comment';

let comment : Comment; 

beforeEach(() =>{
  comment = new Comment();
})

describe('Comment', () => {
  it('should create an instance', () => {
    expect(new Comment()).toBeTruthy();
  });
});

it('should set text , parent_id , user_id and user_equipment correctly', () => {
  const text = "test 12345";
  const parent_id = "1";
  const user_id = "2";
  const user_equipment_id = "1";

  comment.text = text;
  comment.parent_id = parent_id;
  comment.user_equipment_id = user_equipment_id;
  comment.user_id = user_id;

  expect(comment.text).toEqual(text);
  expect(comment.parent_id).toEqual(parent_id);
  expect(comment.user_id).toEqual(user_id);
  expect(comment.user_equipment_id).toEqual(user_equipment_id);
  
});

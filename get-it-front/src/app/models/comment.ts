export class Comment {
    id?: string;
    text?: string;
    user_id?: string;
    user_firstname?: string;
    user_lastname?: string;
    active?: null;
}

import { User, Role } from './user';
let user: User;

beforeEach(() => {
  // Créez une instance du modèle avant chaque test
  user = new User();
});

describe('User', () => {
  it('should create an instance', () => {
    expect(new User()).toBeTruthy();
  });
});

it('should set firstName , lastName , email , phone and role correctly', () => {
  const firstName = 'imen';
  const lastName = 'melki';
  const email = 'test@gmail.com';
  const phone = 123456789;
  const role : Role = Role.ADMIN;

  user.firstName = firstName;
  user.lastName = lastName;
  user.email = email;
  user.phone = phone;
  user.role = role

  expect(user.firstName).toEqual(firstName);
  expect(user.lastName).toEqual(lastName);
  expect(user.email).toEqual(email);
  expect(user.phone).toEqual(phone);
  expect(user.role).toEqual(role);
  
});

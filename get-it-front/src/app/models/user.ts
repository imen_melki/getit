import { Equipment } from "./equipment";

export class User {
    id?: number;
    firstname?: string;
    lastname?: string;
    email?:string;
    username?:string;
    phone_number?: string;
    image?:string
    role?:Role;
    password?:string;
    equipments?: Equipment[];
}

export enum Role {
    ADMIN = "ADMIN",
    TECH = "TECH",
    CONSULTANT = "CONSULTANT"    
}

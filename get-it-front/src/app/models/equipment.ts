export class Equipment {
    id?: Number;
    reference?: string;
    type?: string;
    brand?: string;
    status?: Status;
    location?: string;
    user_id?: Number;
    description?: Text;
    affected_value?:Number;
    comments?: Comment[];
}

export enum Status {
    AVAILABLE = "AVAILABLE",
    ASSIGNED ="ASSIGNED",
    IN_MAINTENANCE ="IN_MAINTENANCE",
    OUTDATED ="OUTDATED"   
}
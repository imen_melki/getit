import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Role } from 'src/app/models/user';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})

export class RoleGuard implements CanActivate {

constructor( private authService:AuthService , private router: Router ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const userRole = this.authService.getUserRole(); // Suppose que votre service d'authentification fournit la méthode getUserRole() pour obtenir le rôle de l'utilisateur

    // Vérifiez le rôle de l'utilisateur
    switch (userRole) {
      case Role.ADMIN:
        return true; // L'accès est autorisé pour les utilisateurs avec le rôle "ADMIN"
      case Role.CONSULTANT:
        return true; // L'accès est autorisé pour les utilisateurs avec le rôle "CONSULTANT"
      case Role.TECH:
        return true; // L'accès est autorisé pour les utilisateurs avec le rôle "technicien"
      default:
        // L'utilisateur n'a pas le rôle requis, redirigez vers une page appropriée ou affichez un message d'erreur
        this.router.navigate(['/unauthorized']);
        return false;
    }
  }
  
}

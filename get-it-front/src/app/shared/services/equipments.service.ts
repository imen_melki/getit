import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Equipment } from 'src/app/models/equipment';
import { environment } from 'src/environments/environment';

const baseUrl = `${environment.apiUrl}/equipment`;

@Injectable({
  providedIn: 'root'
})
export class EquipmentsService {

  constructor(private http: HttpClient) { }

  getEqList() : Observable<Equipment[]>{ 
    return this.http.get<Equipment[]>(baseUrl + "/get_all_equipments")
  }

  getListByUser(user_id : any) : Observable <Equipment[]>{
    return this.http.get<Equipment[]>(environment.apiUrl+`/user/get_equipment_user/${user_id}`)
  }

  addEquipment(equipments:any) : Observable<any[]>{
    return this.http.post<any[]>(baseUrl + "/add_equipments" , {equipments})
  }

  assignEquipment(equipment_id: any, user_id: any) : Observable<any>{
    return this.http.put<any[]>(baseUrl + `/api/users/${user_id}/equipments/${equipment_id}`, {equipment_id, user_id});
  }

  updateEquipment(equipment: Equipment) : Observable<any>{
    return this.http.put<any[]>(baseUrl + `/update_equipments/${equipment.id}`, equipment);
  }

  getEqpStat(){
    return this.http.get(baseUrl + "/stats")
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Role, User } from 'src/app/models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User

  constructor(private http: HttpClient) {
    const storedUser = localStorage.getItem('user');
    this.user = storedUser ? JSON.parse(storedUser) : null;
  }

  login(credentials: any) {
    return this.http.post( environment.apiUrl + '/user/login', credentials );
  }

  isLoggedIn(): boolean {
    // Vérifiez si le token d'authentification est présent dans le stockage local (LocalStorage, cookies, etc.)
    const token = localStorage.getItem('authToken'); // Exemple : utilisation du LocalStorage
  
    // Vérifiez si le token est valide ou non
    // Vous pouvez effectuer des vérifications supplémentaires, telles que la vérification de l'expiration du token, etc.
  
    // Retournez true si l'utilisateur est connecté, false sinon
    return !!token; // Renvoie true si le token existe, false s'il est null ou undefined
  }

  getUserRole(){
    const user = JSON.parse(localStorage.getItem('user')  ?? '').user;
    const userRole = user?.role;
    return userRole ;
  }

  getConnectdUser() {
    const user = JSON.parse(localStorage.getItem('user') ?? '').user;
    return user?.id
  }

  getConnectdUserObj() {
    const user = JSON.parse(localStorage.getItem('user') ?? '').user;
    return user;
  }
}

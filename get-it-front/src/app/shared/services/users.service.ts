import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/models/user';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Equipment } from 'src/app/models/equipment';

const baseUrl = `${environment.apiUrl}/user`;

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient ) { }

  getList() : Observable<User[]> {
    return this.http.get<User[]>(baseUrl + "/get_users_with_equipments")
  }

  addUser(users:any) : Observable<any[]>{
    return this.http.post<any[]>(baseUrl + "/add_users" , {users})
  }

  getMyEquipments(user_id: number) : Observable<Equipment[]>{ 
    return this.http.get<Equipment[]>(baseUrl + `/get_equipment_user/${user_id}`)
  }

  updateUser(user_id: any, user: User) : Observable<any>{
    return this.http.put<any[]>(baseUrl + `/update_user/${user_id}`, user);
  }

  updatePassword(obj: any) : Observable<any>{
    return this.http.post<any[]>(baseUrl + `/change_password`, obj);
  }
}

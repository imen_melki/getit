import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Navigation, Router, ActivatedRoute  } from '@angular/router';
import { Role, User } from 'src/app/models/user';
import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  users: any = [];
  roles: Role[] = [Role.ADMIN, Role.CONSULTANT, Role.TECH]; // Liste des options de rôle
  state: any;
  updateUserForm: FormGroup= new FormGroup({});
  userToUpdate: User = new User();

  constructor(private router: Router, private formBuilder: FormBuilder, private route: ActivatedRoute,
    private toastrService: ToastrService,
    private usersService: UsersService) { }

  ngOnInit(): void {
    this.userToUpdate = JSON.parse(localStorage.getItem('userObj') ?? '').userObj;
    console.log(this.userToUpdate)
    this.updateUserForm = this.formBuilder.group({
      firstname: new FormControl(this.state ? this.state.firstname : this.userToUpdate.firstname, [Validators.required]),
      lastname: new FormControl(this.state ? this.state.lastname : this.userToUpdate.lastname, [Validators.required]),
      email: new FormControl(this.state ? this.state.email : this.userToUpdate.email, [Validators.required, Validators.email]),
      phone_number: new FormControl(this.state ? this.state.phone_number : this.userToUpdate.phone_number, [Validators.required]),
      role: new FormControl(this.state ? this.state.role : this.userToUpdate.role, [Validators.required]),
      username: new FormControl(this.state ? this.state.username : this.userToUpdate.username, [Validators.required]),
      // password: new FormControl(this.state ? this.state.password : this.userToUpdate.password, [Validators.required]),
    });
  }

  getEnumOptions(enumType: any): string[] {
    return Object.keys(enumType).map(key => enumType[key]);
  }

  save() {
    if (this.updateUserForm.invalid) {
      this.toastrService.error('Veuillez vérifier s\'il y a un champ vide!', 'Champs invalide');
    }
    else {
      let updatedUser = {
        firstanme: this.updateUserForm.value.firstanme,
        lastname: this.updateUserForm.value.lastname,
        username: this.updateUserForm.value.username,
        email: this.updateUserForm.value.email,
        phone_number: this.updateUserForm.value.phone_number,
        role: this.updateUserForm.value.role
      }
      this.usersService.updateUser(this.userToUpdate.id, updatedUser).subscribe(() => {
        this.toastrService.success("La modification d'utilisateur a été faite avec succès!", 'Modification avec succès');
        this.router.navigate(['/users']);
      })
    }
  }

}

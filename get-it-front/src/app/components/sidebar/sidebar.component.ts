import { Component, OnInit } from '@angular/core';
import { Role } from 'src/app/models/user';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  userRole:Role

  constructor(private authService: AuthService) {
    this.userRole = this.authService.getUserRole();
  }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Role } from 'src/app/models/user';
import { UsersService } from 'src/app/shared/services/users.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  constructor(private toastrService: ToastrService,
    private usersService: UsersService,
    private formBuilder: FormBuilder) { }

  users: any = [];
  roles: Role[] = [Role.ADMIN, Role.CONSULTANT, Role.TECH]; // Liste des options de rôle

  ngOnInit(): void {
  }

  newUserForm = this.formBuilder.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    email: ['', Validators.required, Validators.email],
    phone_number: ['', Validators.required],
    role: ['', Validators.required],
    username: ['', Validators.required],
    password: ['', Validators.required],

  });

  getEnumOptions(enumType: any): string[] {
    return Object.keys(enumType).map(key => enumType[key]);
  }

  save() {
    if (this.newUserForm.invalid) {
      this.toastrService.error('Veuillez vérifier s\'il y a un champ vide!', 'Champs invalide');
    }
    else {
      this.users.push(this.newUserForm.value)
      this.usersService.addUser(this.users).subscribe(() => {
        this.toastrService.success('votre utilisateur a été ajouté avec succès!', 'Ajouter avec succès');
        this.newUserForm.reset()
      })
    }
  }

  handleFileInput(event: any): void {
    const file: File = event.target.files[0];
    const fileReader: FileReader = new FileReader();

    fileReader.onload = (e: any) => {
      const arrayBuffer: ArrayBuffer = e.target.result;
      const workbook: XLSX.WorkBook = XLSX.read(arrayBuffer, { type: 'array' });
      const worksheet: XLSX.WorkSheet = workbook.Sheets[workbook.SheetNames[0]];
      const jsonData: any[] = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

      for (let i = 1; i < jsonData.length; i++) {
        const row = jsonData[i];
        const firstname = row[0];
        const lastname = row[1];
        const email = row[2];
        const phone_number = row[3].toString();
        const role = row[4];
        const username = row[5];
        const password = row[6];
        const user = {
          firstname,
          lastname,
          email,
          phone_number,
          role,
          username,
          password
        }
        this.users.push(user);
      };
      this.usersService.addUser(this.users).subscribe(res => {
        this.toastrService.success('vos utilisateurs sont ajoutés avec succès', 'Ajouter avec succès');
      })
    }
    fileReader.readAsArrayBuffer(file);
  }

}

import { Component, OnInit, OnDestroy  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/shared/services/users.service';
import { Equipment } from 'src/app/models/equipment';
import { EquipmentsService } from 'src/app/shared/services/equipments.service';
import { Navigation, Router } from '@angular/router';

@Component({
  selector: 'app-assign-equipment',
  templateUrl: './assign-equipment.component.html',
  styleUrls: ['./assign-equipment.component.css']
})
export class AssignEquipmentComponent implements OnInit {

  users: User[] = [];
  equipments: Equipment[] = [];
  state: any;
  equipmentIdToAssign: any;
  assignEquipementForm: FormGroup= new FormGroup({});

  constructor(private router: Router, private formBuilder: FormBuilder, private toastrService: ToastrService, private userServices: UsersService, 
    private equipmentServices: EquipmentsService) {  
      const navigation: Navigation | null = this.router.getCurrentNavigation();
      if (navigation && navigation.extras && navigation.extras.state) {
        this.state = navigation.extras.state['equipmentId'];
      }
    }

  ngOnInit(): void {
    this.equipmentIdToAssign = JSON.parse(localStorage.getItem('equipmentId') ?? '').equipmentId;
    this.getUsers();
    this.getEquipments();
    this.assignEquipementForm = this.formBuilder.group({
      reference: new FormControl(this.state ? this.state : (this.equipmentIdToAssign ? this.equipmentIdToAssign : ''),[Validators.required]),
      username: new FormControl('', [Validators.required]),
      action: new FormControl('',[Validators.required])
    });

  }

  ngOnDestroy() {
    localStorage.removeItem('equipmentId');
  }

  save(){
    if (this.assignEquipementForm.invalid) {
      this.toastrService.error('Veuillez vérifier s\'il y a un champ vide!' , 'Champs invalide');
    }
    else{
      this.equipmentServices
        .assignEquipment(this.assignEquipementForm.value.reference, this.assignEquipementForm.value.username).subscribe(
          res => {
            this.toastrService.success("L'attribution d'équipement a été faite avec succès!", 'Attribution avec succès');
            this.assignEquipementForm.reset();
          }
        )
    }
   }

  getUsers(){
    this.userServices.getList().subscribe(res=>{
      this.users = res;
    })
  }

  getEquipments(){
    this.equipmentServices.getEqList().subscribe(res=>{
      this.equipments = res.filter(r => r.affected_value == 0);
    })
  }

}

import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js/auto';
import { Equipment } from 'src/app/models/equipment';
import { Role, User } from 'src/app/models/user';
import { AuthService } from 'src/app/shared/services/auth.service';
import { EquipmentsService } from 'src/app/shared/services/equipments.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('myCanvas1') canvasRef!: ElementRef;
  @ViewChild('myCanvas2') canvasRef2!: ElementRef;

  userRole: Role;
  userObj: User = new User;

  constructor(private equipmentsService: EquipmentsService,
    private authService: AuthService) {
    this.userRole = this.authService.getUserRole();
    console.log(this.userRole)
    this.userObj = this.authService.getConnectdUserObj();
  }

  oldEqp = 0;
  equipmentList: Equipment[] = [];
  chart: any;
  stats: any;
  data = [];
  showOldEqp = false;

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.createChart();
  }

  createChart() {
    this.equipmentsService.getEqpStat().subscribe(res => {
      this.stats = res
      this.oldEqp = this.stats['equipements_plus_de_3_ans']
      if (!this.showOldEqp) {
        const canvas = this.canvasRef.nativeElement;
        const ctx = canvas.getContext('2d');
        if (ctx) {
          this.chart = new Chart(ctx, {
            type: 'bar', // Type de graphique (par exemple, bar, line, pie, etc.)
            data: {
              labels: ['Totale', 'Disponible', 'Assginé'], // Étiquettes des données
              datasets: [{
                label: 'Operam équipments',
                data: [this.stats['total'], this.stats['stock'], this.stats['sortie']], // Données du graphique
                backgroundColor: 'rgba(75, 192, 192, 0.2)', // Couleur de remplissage des barres
                borderColor: 'rgba(75, 192, 192, 1)', // Couleur des bordures des barres
                borderWidth: 1 // Épaisseur des bordures des barres
              }]
            }
          });
        }
      }
      else {
        const canvas = this.canvasRef2.nativeElement;
        const ctx = canvas.getContext('2d');
        if (ctx) {
          this.chart = new Chart(ctx, {
            type: 'pie', // Type de graphique (par exemple, bar, line, pie, etc.)
            data: {
              labels: ['Equipments', 'Pourcentage plus de 3 ans'], // Étiquettes des données
              datasets: [{
                label: 'tous les équipments',
                data: [100, this.stats['pourcentage_plus_de_3_ans']], // Données du graphique
                backgroundColor: 'rgba(75, 192, 192, 0.2)', // Couleur de remplissage des barres
                borderColor: 'rgba(75, 192, 192, 1)', // Couleur des bordures des barres
                borderWidth: 1 // Épaisseur des bordures des barres
              }]
            }
          });
        }
      }


    })
  }

  toggle() {
    this.showOldEqp = !this.showOldEqp
    this.createChart()
  }
}

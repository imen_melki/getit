import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  users: User[] = [];
  userImgs = [
    { userId: 1, imageUrl: '../assets/img/cyrine.jpg' },
    { userId: 2, imageUrl: '../assets/img/cyrine.jpg' },
    { userId: 3, imageUrl: '../assets/img/otto.jpeg' },
    { userId: 4, imageUrl: '../assets/img/team-2.jpg' },
    { userId: 5, imageUrl: '../assets/img/20220319_124319.jpg' },
    { userId: 6, imageUrl: '../assets/img/imen.png' },
    { userId: 7, imageUrl: '../assets/img/mohamed.jpg' },

  ]

  constructor(private router: Router, private userServices: UsersService) { }

  ngOnInit(): void {   
    this.getUsers() 
  }

  getUsers(){
    this.userServices.getList().subscribe(res=>{
      this.users = res
    })
  }

  getImageUrl(userId: any): string {
    const image = this.userImgs.find(img => img.userId === userId);
    return image ? image.imageUrl : '';
  }

  navigateWithData(user: any) {
    const data = { userObj: user };
    this.router.navigate([`/update-user/${user.id}`], { state: data });
    localStorage.setItem('userObj', JSON.stringify(data));
    console.log(data)
  }

}

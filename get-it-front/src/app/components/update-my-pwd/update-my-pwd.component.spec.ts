import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMyPwdComponent } from './update-my-pwd.component';

describe('UpdateMyPwdComponent', () => {
  let component: UpdateMyPwdComponent;
  let fixture: ComponentFixture<UpdateMyPwdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateMyPwdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UpdateMyPwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

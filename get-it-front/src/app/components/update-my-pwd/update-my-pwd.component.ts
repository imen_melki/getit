import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Navigation, Router, ActivatedRoute  } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/shared/services/users.service';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-update-my-pwd',
  templateUrl: './update-my-pwd.component.html',
  styleUrls: ['./update-my-pwd.component.css']
})
export class UpdateMyPwdComponent implements OnInit {

  state: any;
  updatePasswordForm: FormGroup= new FormGroup({});
  connected_user: User = new User();

  constructor(private router: Router, private formBuilder: FormBuilder, private route: ActivatedRoute, 
    private usersService: UsersService, private authService: AuthService, private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.connected_user = this.authService.getConnectdUserObj();
    this.updatePasswordForm = this.formBuilder.group({
      email: new FormControl({value: this.connected_user?.email, disabled: true},[Validators.required]),
      current_password: new FormControl('', [Validators.required]),
      new_password: new FormControl('',[Validators.required])
    });
  }

  save(){
    if (this.updatePasswordForm.invalid) {
      this.toastrService.error('Veuillez vérifier s\'il y a un champ vide!' , 'Champs invalide');
    }
    else{
      let errorCode : any;
      let updatedPassword = {
        email: this.connected_user?.email,
        current_password: this.updatePasswordForm.value.current_password,
        new_password: this.updatePasswordForm.value.new_password
      }
      this.usersService.updatePassword(updatedPassword).pipe(
        catchError((error: HttpErrorResponse) => {
          // Handle the error here
          errorCode = error.status;       // this.toastrService.error("Le mot de passe actuel saisi n'est pas correct!", 'Mot de passe incorrect');
          return throwError('Something went wrong.'); // Return a custom error message or throw an error
        })
      ).subscribe(
        (response: any) => {
          // Handle the successful response here
          this.toastrService.success('La modification de votre mot de passe a été faite avec succès!', 'Modification avec succès');
          this.updatePasswordForm = this.formBuilder.group({
            email: new FormControl({value: this.connected_user?.email, disabled: true},[Validators.required]),
            current_password: new FormControl('', [Validators.required]),
            new_password: new FormControl('',[Validators.required])
          });
        },
        (error: any) => {
          // Handle the error here
          if (errorCode === 401) {
            this.toastrService.error("Le mot de passe actuel saisi n'est pas correct!", 'Mot de passe incorrect');
          } else {
            this.toastrService.error('Veuillez vérifier s\'il y a un champ vide!' , 'Champs invalide');
          }
        }
      );
    }
  }

}

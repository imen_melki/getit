import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {  Status } from 'src/app/models/equipment';
import { EquipmentsService } from 'src/app/shared/services/equipments.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-new-equipment',
  templateUrl: './new-equipment.component.html',
  styleUrls: ['./new-equipment.component.css']
})
export class NewEquipmentComponent implements OnInit {

  constructor(private toastrService: ToastrService , 
    private equipmentService : EquipmentsService,
    private formBuilder: FormBuilder) { }

  equipments :any = [];
  ngOnInit(): void {
  }

  newEquipementForm = this.formBuilder.group({
    reference: ['' , Validators.required],
    type: ['' , Validators.required],
    status: [Status.AVAILABLE , Validators.required],
    brand: ['' , Validators.required],
    location: ['operam' , Validators.required],
    user_id: [0 , Validators.required],
    description:""
  });

  save(){
    if (this.newEquipementForm.invalid) {
      this.toastrService.error('Veuillez vérifier s\'il y a un champ vide!' , 'Champs invalide');
    }
    else{
      this.equipments.push(this.newEquipementForm.value)
      this.equipmentService.addEquipment(this.equipments).subscribe(()=>{
        this.toastrService.success('votre équipement à était ajouté avec succès!', 'Ajouter avec succès');
        this.newEquipementForm.reset()
      })
      
    }
   }

   handleFileInput(event: any): void {
    const file: File = event.target.files[0];
    const fileReader: FileReader = new FileReader();

    fileReader.onload = (e: any) => {
      const arrayBuffer: ArrayBuffer = e.target.result;
      const workbook: XLSX.WorkBook = XLSX.read(arrayBuffer, { type: 'array' });
      const worksheet: XLSX.WorkSheet = workbook.Sheets[workbook.SheetNames[0]];
      const jsonData: any[] = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

      for (let i = 1; i < jsonData.length; i++) {
        const row = jsonData[i];
        const reference = row[0];
        const type = row[1];
        const status = row[2];
        const brand = row[3];
        const location = row[4];
        const description = row[5];

        const equipment = {
          reference,
          type,
          status,
          brand,
          location,
          description 
        }
        this.equipments.push(equipment);
      };
      this.equipmentService.addEquipment(this.equipments).subscribe(res => {
        this.toastrService.success('vos équipements sont ajoutés avec succès', 'Ajouter avec succès');
      })
    }
    fileReader.readAsArrayBuffer(file);
  }

}

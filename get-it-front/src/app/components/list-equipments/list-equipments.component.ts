import { Component, OnInit } from '@angular/core';
import { Equipment } from 'src/app/models/equipment';
import { EquipmentsService } from 'src/app/shared/services/equipments.service';
import { Role } from 'src/app/models/user';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CommentsService } from 'src/app/shared/services/comments.service';

@Component({
  selector: 'app-list-equipments',
  templateUrl: './list-equipments.component.html',
  styleUrls: ['./list-equipments.component.css']
})
export class ListEquipmentsComponent implements OnInit {

  userRole:Role;
  connectedUser: any;
  equipments: Equipment[] = [];
  filterType: string = "";
  filterLocalisation: string = "";
  filterMarque: string = "";
  filterEtat: string = "";
  commentForm: FormGroup= new FormGroup({});
  textContent: string = '';
  maxLength: number = 250;

  constructor(private router: Router, private authService: AuthService, private equipmentServices: EquipmentsService,
    private formBuilder: FormBuilder, private toastrService: ToastrService, private commentService: CommentsService) {
    this.userRole = this.authService.getUserRole();
    this.connectedUser = JSON.parse(localStorage.getItem('user')  ?? '').user;
    console.log(this.connectedUser.id)
   }

  ngOnInit(): void {   
    this.getEquipments();
    this.commentForm = this.formBuilder.group({
      comment_text: new FormControl('',[Validators.maxLength(250)])
    });
  }

  navigateWithData(equipment: any) {
    const data = { equipmentObj: equipment };
    this.router.navigate([`/update-equipment/${equipment.reference}`], { state: data });
    localStorage.setItem('equipmentObj', JSON.stringify(data));
  }

  getEquipments(){
    if (this.userRole=="ADMIN")
    this.equipmentServices.getEqList().subscribe(res=>{
      this.equipments=res.filter(equipment => {
        equipment.type = equipment.type?.toLowerCase() ;
        equipment.location=equipment.location?.toLowerCase();
        equipment.brand=equipment.brand?.toLowerCase();
        if (
          (!this.filterType || equipment.type === this.filterType) &&
          (!this.filterLocalisation || equipment.location === this.filterLocalisation) &&
          (!this.filterMarque || equipment.brand === this.filterMarque) &&
          (!this.filterEtat || equipment.status === this.filterEtat)
        ) {
          return true; 
        } else {
          return false; 
        }
    })
    })
    else{
      const userId = this.authService.getConnectdUser()
      this.equipmentServices.getListByUser(userId).subscribe(res=>{
        this.equipments=res
      })
    }
  }

  to_assign(equipment_id: any) {
    const data = { equipmentId: equipment_id };
    this.router.navigate([`/assign-equipment`], { state: data });
    localStorage.setItem('equipmentId', JSON.stringify(data));
  }

  //COMMENT MODAL
  updateCharactersCount() {}

  to_comment(equipment_id: any) {
    localStorage.setItem("equipmentId", equipment_id);
  }

  sendComment() {
    if (this.commentForm.invalid) {
      this.toastrService.error('Le commentaire ne doit pas dépasser 250 caractères!' , 'Champ invalide');
    }
    else{
      let comment = {
        text : this.commentForm.value.comment_text,
        equipment_id : localStorage.getItem("equipmentId"),
        user_id : this.connectedUser.id
      }
      this.commentService.addComment(comment).subscribe(res => {
        this.toastrService.success("Votre commentaire a été envoyé avec succès!", 'Envoi avec succès');
        this.reset();
      })
    }
  }

  reset() {
    this.commentForm.get('comment_text')?.setValue('');
  }


}

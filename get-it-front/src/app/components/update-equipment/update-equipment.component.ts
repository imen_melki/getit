import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Navigation, Router, ActivatedRoute  } from '@angular/router';
import { Equipment } from 'src/app/models/equipment';
import { EquipmentsService } from 'src/app/shared/services/equipments.service';
import { Role } from 'src/app/models/user';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-update-equipment',
  templateUrl: './update-equipment.component.html',
  styleUrls: ['./update-equipment.component.css']
})
export class UpdateEquipmentComponent implements OnInit {

  isDisabled : boolean;
  userRole:Role;
  state: any;
  updateEquipementForm: FormGroup= new FormGroup({});
  equipmentToUpdate: Equipment = new Equipment();

  constructor(private router: Router, private formBuilder: FormBuilder, private route: ActivatedRoute, 
    private authService: AuthService, private equipmentServices: EquipmentsService, private toastrService: ToastrService) {
      this.userRole = this.authService.getUserRole();
      this.isDisabled = this.userRole !== 'ADMIN';
    const navigation: Navigation | null = this.router.getCurrentNavigation();
    if (navigation && navigation.extras && navigation.extras.state) {
      this.state = navigation.extras.state['equipmentObj'];
    }
  }

  ngOnInit(): void {  
    this.equipmentToUpdate = JSON.parse(localStorage.getItem('equipmentObj') ?? '').equipmentObj;
    this.updateEquipementForm = this.formBuilder.group({
      ref: new FormControl({value: this.state ? this.state.reference : this.equipmentToUpdate.reference, disabled: this.isDisabled},[Validators.required]),
      type: new FormControl({value: this.state ? this.state.type : this.equipmentToUpdate.type, disabled: this.isDisabled}, [Validators.required]),
      status: new FormControl(this.state ? this.state.status : this.equipmentToUpdate.status,[Validators.required]),
      brand: new FormControl({value: this.state ? this.state.brand : this.equipmentToUpdate.brand, disabled: this.isDisabled},[Validators.required]),
      description: new FormControl({value: this.state ? this.state.description : this.equipmentToUpdate.description, disabled: this.isDisabled},[]),
    });
  }

  save(){
    if (this.updateEquipementForm.invalid) {
      this.toastrService.error('Veuillez vérifier s\'il y a un champ vide!' , 'Champs invalide');
    }
    else{
      let updatedEquipment = {
        id: this.equipmentToUpdate.id,
        reference: this.updateEquipementForm.value.ref,
        type: this.updateEquipementForm.value.type,
        status: this.updateEquipementForm.value.status,
        brand: this.updateEquipementForm.value.brand,
        description: this.updateEquipementForm.value.description
      }
      this.equipmentServices
        .updateEquipment(updatedEquipment).subscribe(
          res => {
            this.toastrService.success("La modification d'équipement a été faite avec succès!", 'Modification avec succès');
            this.router.navigate(['/equipments']);
          }
        )
    }
  }


}

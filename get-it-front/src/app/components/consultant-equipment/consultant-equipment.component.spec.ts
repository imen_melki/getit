import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CONSULTANTEquipmentComponent } from './CONSULTANT-equipment.component';

describe('CONSULTANTEquipmentComponent', () => {
  let component: CONSULTANTEquipmentComponent;
  let fixture: ComponentFixture<CONSULTANTEquipmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CONSULTANTEquipmentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CONSULTANTEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

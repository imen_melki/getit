import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Equipment } from 'src/app/models/equipment';
import { EquipmentsService } from 'src/app/shared/services/equipments.service';
import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-consultant-equipment',
  templateUrl: './consultant-equipment.component.html',
  styleUrls: ['./consultant-equipment.component.css']
})
export class ConsultantEquipmentComponent implements OnInit {

  equipments: Equipment[] = [];
  commantaire = ""

  constructor(private usersService: UsersService, private router: Router, private equipmentServices: EquipmentsService) { }

  ngOnInit(): void {   
  //  this.usersService.getMyEquipments(1).subscribe(res => {
  //   this.equipments = res;
  //  })
  }

  navigateWithData(equipment: any) {
    const data = { equipmentObj: equipment };
    this.router.navigate([`/update-equipment/${equipment.reference}`], { state: data });
    localStorage.setItem('equipmentObj', JSON.stringify(data));
  }

  getEquipments(){
    this.equipmentServices.getEqList().subscribe(res=>{
      this.equipments=res
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  email!: string;
  password!: string;

  constructor(private appComponent: AppComponent,
    private authService: AuthService,
    private router: Router) {
    this.appComponent.showHeaderFooter = false;
  }


  ngOnInit(): void {
  }

  onSubmit() {
    const credentials = { email: this.email, password: this.password };
    this.authService.login(credentials).subscribe(res=>{
      localStorage.setItem('user', JSON.stringify(res));
      this.appComponent.showHeaderFooter = true;
      this.router.navigate([`/home`]);
    })
  }

}

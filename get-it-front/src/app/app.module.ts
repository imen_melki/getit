import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './components/user/user.component';
import { EquipmentComponent } from './components/equipment/equipment.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { NewEquipmentComponent } from './components/new-equipment/new-equipment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListEquipmentsComponent } from './components/list-equipments/list-equipments.component';
import { ListUsersComponent } from './components/list-users/list-users.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { UpdateEquipmentComponent } from './components/update-equipment/update-equipment.component';
import { AssignEquipmentComponent } from './components/assign-equipment/assign-equipment.component';
import { ConsultantEquipmentComponent } from './components/consultant-equipment/consultant-equipment.component';

import { LoginComponent } from './login/login.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';
import { UpdateMyPwdComponent } from './components/update-my-pwd/update-my-pwd.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    EquipmentComponent,
    SidebarComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    NewEquipmentComponent,
    ListEquipmentsComponent,
    ListUsersComponent,
    ConsultantEquipmentComponent,
    NewUserComponent,
    UpdateEquipmentComponent,
    AssignEquipmentComponent,
    UpdateUserComponent,
    UpdateMyPwdComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 15000, // 15 seconds
      closeButton: true,
      progressBar: true,
    }),
    FormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { HomeComponent } from './components/home/home.component';
import { NewEquipmentComponent } from './components/new-equipment/new-equipment.component';
import { ListUsersComponent } from './components/list-users/list-users.component';
import { ListEquipmentsComponent } from './components/list-equipments/list-equipments.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { UpdateEquipmentComponent } from './components/update-equipment/update-equipment.component';
import { AssignEquipmentComponent } from './components/assign-equipment/assign-equipment.component';
import { ConsultantEquipmentComponent } from './components/consultant-equipment/consultant-equipment.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';
import { UpdateMyPwdComponent } from './components/update-my-pwd/update-my-pwd.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path : "", component : LoginComponent},
  {path : "home", component : HomeComponent},

  {path : "users", component : ListUsersComponent},
  {path : "equipments", component : ListEquipmentsComponent},
  {path : "new-equipment", component : NewEquipmentComponent},
  {path : "consultant-equipments", component : ConsultantEquipmentComponent},
  {path : "new-user", component : NewUserComponent},
  {path : "update-equipment/:ref", component : UpdateEquipmentComponent},
  {path : "assign-equipment", component : AssignEquipmentComponent},
  {path : "update-user/:id", component : UpdateUserComponent},
  {path : "update-my-password", component : UpdateMyPwdComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
})
export class AppRoutingModule { }
